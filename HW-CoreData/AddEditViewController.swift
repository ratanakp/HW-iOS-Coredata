//
//  AddEditViewController.swift
//  HW-CoreData
//
//  Created by Ratanak Phang on 12/20/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class AddEditViewController: UIViewController {

    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var titleTextView: UITextView!
    
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    @IBOutlet weak var moreBarButton: UIBarButtonItem!
    
    
    var note: Note?
    var status: Bool!
    var ind: Int?
    
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        note = Note()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParentViewController {
            
            // add new
            if status == true {
                let note = Note(context: context)
                if self.titleTextView.text == "" || self.titleTextView.text == nil{
                    self.titleTextView.text = "unitled"
                }
                note.title = self.titleTextView.text
                note.desc = self.noteTextView.text
                
                //appDelegate.saveContext()
                
                try! context.save()
                
            }
            else {
                print("====> THis is: \(String(describing: ind))")
                let note: [Note]?
                note = try? context.fetch(Note.fetchRequest())
                note![ind!].title = self.titleTextView.text
                note![ind!].desc = self.noteTextView.text
                
                appDelegate.saveContext()
            
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITextView.appearance().tintColor = UIColor(displayP3Red: 112/255, green: 201/255, blue: 1, alpha: 1)
        
        if note == nil {
            titleTextView.placeholder = "Title Here"
            noteTextView.placeholder = "Note Here!"
            status = true // Add new
        }
        else {
            titleTextView.text = note!.title
            noteTextView.text = note!.desc
            status = false // Upadate
        }
        
        titleTextView.layer.borderWidth = 0.3
        titleTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        noteTextView.layer.borderWidth = 0.3
        noteTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        let pinButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_flash_on"), style: .plain, target: self, action: nil)
        let touchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_touch_app"), style: .plain, target: self, action: nil)
        let archiveButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_archive"), style: .plain, target: self, action: nil)
        
        pinButton.tintColor = UIColor(displayP3Red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        touchButton.tintColor = UIColor(displayP3Red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        archiveButton.tintColor = UIColor(displayP3Red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        
        /*let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_keyboard_arrow_left"), style: .plain, target: self, action: nil)
        self.navigationItem.leftBarButtonItem = leftButton*/
        
       // self.navigationItem.rightBarButtonItem?.tintColor = UIColor.black
        
        self.navigationItem.rightBarButtonItems = [archiveButton, touchButton, pinButton]
        
       
    }
    
  
    @IBAction func addBarButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { (action) in
            print("Take Photo")
        }))
        alert.addAction(UIAlertAction(title: "Choose Image", style: .default, handler: { (action) in
            print("Choose Image")
        }))
        alert.addAction(UIAlertAction(title: "Drawing", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Recording", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Checkboxes", style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func moreBarButtonAction(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action) in
            print("Delete")
        }))
        alert.addAction(UIAlertAction(title: "Make a Copy", style: .default, handler: { (action) in
            print("Make a Copy")
        }))
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Collaborators", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Labels", style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}



//Custom placeholder for text view
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.description.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.description.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
}

