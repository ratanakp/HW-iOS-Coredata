//
//  CustomCollectionViewCell.swift
//  HW-CoreData
//
//  Created by Ratanak Phang on 12/20/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var note: UILabel!
    
    func AddNoteTitle(title: String, note: String) {
        self.title.text = title
        self.note.text = note
    }
}
