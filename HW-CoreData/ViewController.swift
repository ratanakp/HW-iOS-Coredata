//
//  ViewController.swift
//  HW-CoreData
//
//  Created by Ratanak Phang on 12/20/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

struct Content {
    var title: String?
    var note: String?
}



class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var navBarItem: UINavigationItem!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    @IBOutlet weak var noteYouAdd: UILabel!
    @IBOutlet weak var takeNoteLabel: UILabel!
    
    var allNote: [Note]?
    var searchController: UISearchController!
    
    var countNote: Int!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    
    
    
    // Receive action
    @objc func labelAction(gr:UITapGestureRecognizer)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let contentForStoryboard = storyboard.instantiateViewController(withIdentifier: "DetailStoryBoard") as! AddEditViewController
        self.navigationController?.pushViewController(contentForStoryboard, animated: true)

        print("Something")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.myCollectionView.reloadData()
        getNotes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noteYouAdd.isHidden = false
        // Do any additional setup after loading the view, typically from a nib.
        
        UISearchBar.appearance().tintColor = UIColor.red
        UISearchBar.appearance().backgroundColor = UIColor.clear
        
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(labelAction))
        takeNoteLabel.addGestureRecognizer(tap)
        takeNoteLabel.isUserInteractionEnabled = true
        tap.delegate = self
        // Remember to extend your class with UIGestureRecognizerDelegate
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = true
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
        
        
        navigationItem.searchController = searchController
        
        navigationItem.hidesSearchBarWhenScrolling = true
        
        
        //UINavigationBar.appearance().tintColor = UIColor(displayP3Red: 255/255, green: 187/255, blue: 0/255, alpha: 1)
        
        UIApplication.shared.statusBarStyle = .lightContent
        
//        let searchButton = UIButton(type: .custom)
//        searchButton.setImage(#imageLiteral(resourceName: "img-search"), for: .normal)
//        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        //let searchBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "img-search"), style: .plain, target: self, action: nil)
        
        //let viewBarButton = UIBarButtonItem(customView: searchButton)
        //let viewBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "img-view"), style: .plain, target: self, action: nil)
        
        
        //self.navBarItem.rightBarButtonItems = [searchBarButton, viewBarButton]
        
        self.myCollectionView.delegate = self
        self.myCollectionView.dataSource = self
        
       
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        let width = UIScreen.main.bounds.width / 2 - 15
        layout.itemSize = CGSize(width: width, height: 150)
        
        myCollectionView.collectionViewLayout = layout
        
        
        
        
        // Long press recognizr
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5 // 1 second press
        longPressGesture.allowableMovement = 15 // 15 points
        longPressGesture.delaysTouchesBegan = false
        self.myCollectionView.addGestureRecognizer(longPressGesture)

        longPressGesture.delegate = self
        // End long press recegnizer
        
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
    }
    
    // Function for handle long press
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let position = gestureRecognizer.location(in: self.myCollectionView)
        let indexPath = self.myCollectionView.indexPathForItem(at: position)
        
        if let index = indexPath {
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.context.delete(self.allNote![index.item])
                try! self.context.save()
                self.getNotes()
                
                
                
                // Update Collection View
                self.myCollectionView.performBatchUpdates({
                    self.myCollectionView.deleteItems(at: [index])
                    
                    // This is should alert something when complete!!!
                }, completion: nil)
                
                
                print("Delete clicked")
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                print("Cancel clicked")
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    // End function for handle long press
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if countNote == 0 {
            print(countNote)
            self.noteYouAdd.isHidden = false
        }
        else {
            self.noteYouAdd.isHidden = true
        }
        
        return countNote
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CustomCollectionViewCell
        
        //cell.backgroundColor = UIColor.black
        cell.layer.borderWidth = 0.3
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.backgroundColor = UIColor.white.cgColor
        
        
        
        cell.title.text = allNote![indexPath.row].title
        cell.note.text = allNote![indexPath.row].desc
        
       
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let contentForStoryboard = storyboard.instantiateViewController(withIdentifier: "DetailStoryBoard") as! AddEditViewController
        
        contentForStoryboard.note = allNote![indexPath.row]
        contentForStoryboard.ind = indexPath.row
        
        self.navigationController?.pushViewController(contentForStoryboard, animated: true)
    }
    
    
    func getNotes() {
        self.allNote = try? context.fetch(Note.fetchRequest())
        countNote = self.allNote?.count
    }
    
}






























